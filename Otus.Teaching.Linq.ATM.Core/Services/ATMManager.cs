﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services.Exceptions;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        public User GetUser(string login, string password)
        {
            try
            {
                return Users.First(u => u.Login == login && u.Password == password);
            }
            catch (InvalidOperationException)
            {
                throw new UserNotFoundException();
            }
        }

        public IEnumerable<Account> GetUserAccounts(User user)
        {
            return Accounts.Where(a => a.UserId == user.Id);
        }

        public IEnumerable<IGrouping<Account, OperationsHistory>> GetUserAccountsWithHistory(User user)
        {
            return from account in Accounts
                   where account.UserId == user.Id
                   join op in History on account.Id equals op.AccountId
                   group op by account;
        }

        public IEnumerable<Tuple<OperationsHistory, User>> GetInputCashOperationHistory()
        {
            return from op in History
                   where op.OperationType == OperationType.InputCash
                   join account in Accounts on op.AccountId equals account.Id
                   join user in Users on account.UserId equals user.Id
                   select Tuple.Create(op, user);
        }

        public IEnumerable<User> GetUsersWithCashOverAmount(decimal amount)
        {
            return from user in Users
                   join account in Accounts on user.Id equals account.UserId
                   group account.CashAll by user into g
                   where g.Sum() > amount
                   select g.Key;
        }
    }
}
