﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Services.Exceptions
{
    public class UserNotFoundException : Exception
    {
    }
}
