﻿using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Extensions
{
    public static class EntityExtensions
    {
        public static string ConvertToString(this User user)
        {
            string fields = string.Join(", ",
                $"Id: {user.Id}",
                $"Name: {user.FirstName} {user.SurName} {user.MiddleName}",
                $"PassportSeriesAndNumber: {user.PassportSeriesAndNumber}"
            );
            return $"User: {{ {fields} }}";
        }

        public static string ConvertToString(this Account account)
        {
            string fields = string.Join(", ",
                $"Id: {account.Id}",
                $"OpeningDate: {account.OpeningDate}",
                $"CashAll: {account.CashAll}",
                $"UserId: {account.UserId}"
            );
            return $"Account: {{ {fields} }}";
        }

        public static string ConvertToString(this OperationsHistory history)
        {
            string fields = string.Join(", ",
                $"Id: {history.Id}",
                $"OperationDate: {history.OperationDate}",
                $"OperationType: {history.OperationType}",
                $"CashSum: {history.CashSum}",
                $"AccountId: {history.AccountId}"
            );
            return $"OperationsHistory: {{ {fields} }}";
        }
    }
}
