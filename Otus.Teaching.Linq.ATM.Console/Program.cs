﻿using SysConsole = System.Console;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Extensions;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System;
using Otus.Teaching.Linq.ATM.Core.Services.Exceptions;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            SysConsole.WriteLine("Старт приложения-банкомата..." + Environment.NewLine);

            var atmManager = CreateATMManager();


            SysConsole.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю;" + Environment.NewLine);

            var user = atmManager.GetUser("snow", "111");
            SysConsole.WriteLine(user.ConvertToString());

            try
            {
                var missingUser = atmManager.GetUser("snow", "zzz");
                SysConsole.WriteLine(missingUser.ConvertToString());
            }
            catch (UserNotFoundException)
            {
                SysConsole.WriteLine("User not found");
            }


            SysConsole.WriteLine();
            SysConsole.WriteLine("2. Вывод данных о всех счетах заданного пользователя;" + Environment.NewLine);

            var accounts = atmManager.GetUserAccounts(user);
            foreach (var a in accounts)
            {
                SysConsole.WriteLine(a.ConvertToString());
            }


            SysConsole.WriteLine();
            SysConsole.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;" + Environment.NewLine);

            var accountsWithHistory = atmManager.GetUserAccountsWithHistory(user);
            foreach (var ah in accountsWithHistory)
            {
                var account = ah.Key;
                var operations = ah.AsEnumerable();

                SysConsole.WriteLine(account.ConvertToString());
                foreach (var op in operations)
                {
                    SysConsole.WriteLine(op.ConvertToString());
                }
                SysConsole.WriteLine();
            }


            SysConsole.WriteLine();
            SysConsole.WriteLine("4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;" + Environment.NewLine);

            var inputCashOperationHistory = atmManager.GetInputCashOperationHistory();
            foreach (var operationWithUser in inputCashOperationHistory)
            {
                var operation = operationWithUser.Item1;
                var operationUser = operationWithUser.Item2;
                SysConsole.WriteLine(operation.ConvertToString());
                SysConsole.WriteLine("    " + operationUser.ConvertToString());
            }


            SysConsole.WriteLine();
            SysConsole.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся извне и может быть любой);" + Environment.NewLine);

            var usersWithCashOver15k = atmManager.GetUsersWithCashOverAmount(15_000m);
            foreach (var u in usersWithCashOver15k)
            {
                SysConsole.WriteLine(u.ConvertToString());
            }


            SysConsole.WriteLine();
            SysConsole.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}
